module gitlab.com/grprado/upinger

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/grprado/ping v0.0.0-20201226172512-e8434338dd8d
)
