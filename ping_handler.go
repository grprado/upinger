//    upinger - micro pinger - a microservice ping interface
//    Copyright (C) 2020  Guilherme Prado
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/grprado/ping"
	"gitlab.com/grprado/upinger/util"
	"net/http"
	"time"
)

type PingHandler struct {
}

func (p PingHandler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)

	pinger := ping.New(vars["addr"])
	pinger.Count = util.ClampInt(util.IntValue(request, "count", 1), 1, 10)
	pinger.Timeout = time.Duration(util.ClampInt(util.IntValue(request, "timeout", 10000), 1, 30000)) * time.Millisecond
	err := pinger.Run()

	if err == nil {
		response.WriteHeader(http.StatusOK)
		json.NewEncoder(response).Encode(pinger.Statistics())
	} else {
		response.WriteHeader(http.StatusInternalServerError)
	}
}
