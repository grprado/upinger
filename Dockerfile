FROM golang:1.15-alpine AS builder
WORKDIR /src
ENV CGO_ENABLED=0
COPY go.mod ./
RUN go mod download
COPY . .
RUN go build


FROM alpine:3.12 AS upinger
WORKDIR /upinger
COPY --from=builder /src/upinger ./
EXPOSE 8000
ENTRYPOINT /upinger/upinger