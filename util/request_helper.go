//    upinger - micro pinger - a microservice ping interface
//    Copyright (C) 2020  Guilherme Prado
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package util

import (
	"strconv"
)

type FormValuer interface {
	FormValue(key string) string
}

// IntValue return the value from request.FormValue(key) as int or defaultValue if
// that value is not a valid int
func IntValue(request FormValuer, key string, defaultValue int) int {
	value, err := strconv.ParseInt(request.FormValue(key), 10, 32)
	if err != nil {
		return defaultValue
	}
	return int(value)
}
