//    upinger - micro pinger - a microservice ping interface
//    Copyright (C) 2020  Guilherme Prado
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package util

import (
	"testing"
)

type formValueMock struct {
}

func (*formValueMock) FormValue(key string) string {
	if key == "a" {
		return "10"
	}
	return ""
}

var fvm = formValueMock{}

func TestIntValue(t *testing.T) {
	if IntValue(&fvm, "a", 1) != 10 {
		t.Error("Should return 10")
	}
	if IntValue(&fvm, "b", 5) != 5 {
		t.Error("Should have returned the default value")
	}
}
