//    upinger - micro pinger - a microservice ping interface
//    Copyright (C) 2020  Guilherme Prado
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

package util

import "testing"

func TestMaxInt(t *testing.T) {
	if MaxInt(1, 3) != 3 {
		t.Error("Max should return 3 instead of 1")
	}
}

func TestMinInt(t *testing.T) {
	if MinInt(1, 3) != 1 {
		t.Error("Max should return 1 instead of 3")
	}
}

func TestClamp(t *testing.T) {
	if ClampInt(20, 1, 15) != 15 {
		t.Error("Clamp should return 15")
	}

	if ClampInt(20, 50, 150) != 50 {
		t.Error("Clamp should return 50")
	}

	if ClampInt(20, 1, 35) != 20 {
		t.Error("Clamp should return 20")
	}
}
